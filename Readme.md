An SDK Generator for Blacklight : Retribution. 

This project has a Branch for each version of BL:R.

> Note:  Currently the generated SDK isn't usable!

## Usage

You can dump your own SDK using this project.

### requirements

To follow the steps below you need to have:

- Visual Studio 2017 (with BuildTools)
- A Blacklight: Retribution Client
- The GNames and GObjects offsets

### build dll

1. Check the patterns & masks of `GObjects` and `GNames` in `GameDefines.h`.
2. run `Release x86`

### inject to client

We have to inject the .dll to a running BL:R client, which will immediatly begin to dump all UObjects into the folder specified by `SDK_BASE_DIR`in `TFL_SdkGen.h`.



## Bugs

The're currently a few problems with this generator so the generated SDK isn't working properly. See the issues for progress on that.